---
title: Week 3, Dynamic Programming
subtitle: Algorithm Design, Analysis, and Complexity, Week 3
date: 2017-05-21T18:00:00-00:00
comments: false
katex: true
markup: "mmark"
toc: true
---

**May 21, 2019 (Week 3): Dynamic Programming**

**Problem***(Fibonacci Series)*.

Recall the pattern: $$1, 1, 2, 3, 5, 8, 13, 21, 34, 55, \ldots$$

Notice the following runs in exponential time.

```python
def fib(n):
    if n < 2:
        return 1
    else:
        return fib(n-1) + fib(n-2)
```

However, we can do this iteratively for an $$\mathcal{O}(n)$$ runtime instead.

```python
def fib-sequence(n):
    fib = [0] * n
    fib[0] = fib[1] = 1
    for k in range(2, n):
        fib[k] = fib[k-1] + fib[k-2]
    return fib
```

**Remark**. This is more efficient but is a *massive waste* of space, while if we need to call multiple times we would need to repeat the computation.

```python
F = [1, 1]

def memoized_fib(n, F):
    if len(F) >= n:
        return F[n-1]
    for k in range(len(F), n):
        F[k] = F[k-1] + F[k-2]
    return F[n-1]
```

**Definition***(Memoization)*. Storing previous computations in an array.

This reduces our runtime for multiple calls: if we have $$\mathcal{N}$$ numbers to query and they are all bounded by $$\mathcal{M}$$, the memoization runs in $$\mathcal{O}(\mathcal{N} + \mathcal{M})$$.



# Weighted Interval Scheduling

You have a set of $$n$$ jobs $$\mathcal{J}$$ where every job $$j$$ has a priority $$p_i$$ and interval of time $$[s_i, f_i)$$.

Select a subset $$S \subseteq \mathcal{J}$$ of mutually compatible jobs to maximize $$\sum_{j \in S} p_j$$.

## Preprocessing

Assume the requests are sorted by the finishing time.

Define $$P(j)$$ for the request $$j$$ as the largest index $$i<j$$ such that the request is compatible.

Let $$O$$ be an optimal solution.

If $$n\in O$$, all we need to do now is fine an optimal subset for jobs $$\{1, \ldots, P(n)\}$$.
Otherwise, you find the optimal subset for jobs $$\{1, \ldots, n-1\}$$.

Let $$\theta_j$$ denote an optimal solution to the problem consisting of requests $$1\ldots j$$.
Let $$\text{OPT}_j$$ denote the value of this solution.

Notice,

1. $$j \in \Theta_j \implies \text{OPT}_j = v_j + \text{OPT}_{p(j)}$$
2. $$j \not\in \Theta_j \implies \text{OPT}_j = \text{OPT}_{j-1}$$

Consequently, notice for every $$j$$, \text{OPT}_j = max \{v_j + \text{OPT}_{P(j)}, \text{OPT}_{j-1}\}.
Moreover, $$j \in \Theta_j \iff v_j + \text{OPT}_{P(j)} \geq \text{OPT}_{j-1}$$.

```
def compute_opt(j):
    if j == 0:
        return 0
    else:
        return max(v_j + compute_opt(P(j)), compute_opt(j-1))
```

Now make it iterative and you should be good.

**Claim**. This algorithm is correct... by induction!

**Base Case**. If you have no requests, there is no value, so we are correct.

**Induction Hypothesis**. Assume `compute_opt` is valid for all $$i < j$$. It will work for inputs of size $$j$$.

We either include $$j$$ in our set, or we don't.
So,

$$\text{OPT}(j) = \max(v_j +  \text{OPT}(p(j)), \text{OPT}(j-1)} = \texttt{compute-opt}(j)$$.

So our algorithm is correct, but if we do it in this form, we have $$T(n) = T(n-1) + T(p(n))$$, which can blow up to $$T(n) = 2T(n-1) = 2^n$$ in the worst case.

How do we fix this? Memoization!

```python
def compute-opt-memoization(j, M):
    if j == 0:
        return 0
    elif M[j] != None:
        return M[j]
    else:
        M[j] = compute-opt(j)
	return M[j]
```
Which runs in $$\Theta(n)$$.

# Dynamic Programming tldr

1. Separate your problem into not neccessarily disjoint subproblems, and relate them.
2. Solve your subproblems.
3. Relate the solutions of the subproblems to your original problem's solution.
4. Turn it into an iterative problem.

# 5 Steps

1. Define the optimal substructure/recursive structure (this is where you need the proof of correctness).
2. Define a memoization.
3. Define the recurrence relation in terms of our array.
4. Create a bottom-up iterative algorithm.
5. Relate the solutions of the subproblems to the solution of your original problem.

