---
title: year 1 info [old]
subtitle: boundless post
date: 2017-05-03T23:00:00-00:00
comments: false
---
This guide mainly applies if you are a *first year* computer science (or perhaps math) student at UTSG.
The courses are somewhat the same, but I can't guarantee anything for UTM/SC/any other uni.

- [Housekeeping](#housekeeping)
- [So... What is POSt?](#so-what-is-post-)
  * [How do I get CS POSt?](#how-do-i-get-cs-post-)
  * [So what does it actually mean if I make it in for CS from high school?](#so-what-does-it-actually-mean-if-i-make-it-in-for-cs-from-high-school-)
  * [Can I try again if I don’t make it the first time?](#can-i-try-again-if-i-don-t-make-it-the-first-time-)
  * [Links](#links)
- [Quizes and stuff.](#quizes-and-stuff)
  * [Burbulla's](#burbulla-s)
  * [UfT Resources](#uft-resources)
- [Random Course Lectures and Open Source Books](#random-course-lectures-and-open-source-books)
  * [Course Notes](#course-notes)
  * [Open Source Textbooks and Misc.](#open-source-textbooks-and-misc)
- [Other Stuff](#other-stuff)
  * [Thanks](#thanks)

## Housekeeping

- I own nothing linked here - and I trust people use these resources here for
  *personal use*. Don't infringe on intellectual property and sell the
  resources or whatever. Making POSt is cool. Ripping people off isn't.
- I highly recommend archiving anything you really need. I'm not giving any
  archival links here, but you should be able to download/keep a copy of things you want.
- Feel free to make push requests/ask me on Discord (`re#4111`) if you want to add something.
  If you make a push, message me anyways since I don't know if I get email notifs.

## So... What is POSt?

POSt stand for Program Of Study. 
Simply put, when you make it into the faculty of Arts and Science (or UTSC/UTM),
you are actually `undeclared`, regardless of whatever you applied for.

Ignore if you're not in those faculties/are otherwise in a program where they
explicitly state first years enter the program directly. 
**This includes the engies.** Your "POSt" is trying to not design buildings 
that will lead to Prof. Cohen confiscating your iron ring.

If you are reading this, you prolly care about CS POSt more.

### How do I get CS POSt?

The requirement is different for every campus..

- SG: ~90 in Intro to CS 2 and Discrete Mathematics (CSC148 and CSC165). Notice
  that this is *not* a guarantee, but instead they order everyone who applies
  and takes the top 550ish spots.
- SC: 3.0 in 6 courses. They are calc 1 (MATA31), calc 2 (MATA37), discrete 
  maths (CSCA67), lin alg 1 (MATA22) intro to CS 1 (CSCA08), intro to CS 2 
  (CSCA48). Note that A08 and A31 are weighted at 0.5.
- M: ~75 in (I think) CSC148 and MAT102. IIRC you also need a 3.0 CGPA. 
  Not too sure about this one, so go check the links at the bottom.

### So what does it actually mean if I make it in for CS from high school?

You get autoenrolled in courses. That's literally it for now. UTSG used to make
the in-stream cutoff lower, but not anymore.

### Can I try again if I don’t make it the first time?

Firstly, you can always drop and retake courses that you’re doing bad in.
For SG (and maybe M, idk) you can retake any course once for the sake of Post.
For SC, you can’t retake any course you passed so be careful.

### Links

[UTSG](http://web.cs.toronto.edu/program/ugrad/admission.htm)

[UTSC](https://www.utsc.utoronto.ca/cms/computer-science-1)

[UTM](https://www.utm.utoronto.ca/math-cs-stats/current-students/computer-science)

# Quizes and stuff.

## Burbulla's

*IIRC, the author is an eng prof, but... close enough. Give em a shot.
Solutions are somewhere on his site.*

[Basic Algebra](https://www.math.toronto.edu/burbulla/basicalgebra.pdf)

[Functions](https://www.math.toronto.edu/burbulla/functions.pdf)

[Trig](https://www.math.toronto.edu/burbulla/trigonometry.pdf)

[Logs and Exponentials](https://www.math.toronto.edu/burbulla/logsandexponentials.pdf)

## UfT Resources

*From Gerty's domain itself.*

[Math Dept's Math Prep](https://www.math.toronto.edu/preparing-for-calculus/)

# Random Course Lectures and Open Source Books

*YMMV. Maybe helpful*.

### Course Notes

#### Unknown

*I don't know who wrote these, but prolly helpful.*

[CSC148](https://www.teach.cs.toronto.edu/~csc148h/fall/notes/index.html)

#### Tyler Holden 

*(Currently a UTM prof, writes nice Calc notes. Love this guy.)*

[MAT102 (Proof Primer + UTM POSt Course)](http://home.tykenho.com/LectureNotes102.pdf)

[MAT137 (Calculus! Highly recommended)](http://home.tykenho.com/LectureNotes137_Preview.pdf)

[MAT223 (Linear Algebra)](https://mcs.utm.utoronto.ca/~tholden/LectureNotes223.pdf)

#### David Liu & Toniann Pitassi

*(165 Prof IIRC.)*

[CSC165 **(POSt Course)**](https://www.teach.cs.toronto.edu/~csc165h/winter/resources/csc165_notes.pdf)

#### Vassos Hadzilacos

*(One of the upper year profs at UTSC for CS.)*

[CSC236/240/B36 (Theory of Computation. 2nd Year. May be a bit dry, but I like it.)](http://www.cs.toronto.edu/~vassos/b36-notes/notes.pdf)

### Open Source Textbooks and Misc.

[Interactive Linear Algebra](http://immersivemath.com/ila/index.html)

## Other Stuff

*Educational value not guaranteed.*

[Essence of Linear Algebra - 3Blue1Brown (Godsend)](https://www.youtube.com/watch?v=fNk_zzaMoSs&list=PLZHQObOWTQDPD3MizzM2xVFitgF8hE_ab)

[Bahen Roast from Eng Prof - 100% Comedy Value](https://www.youtube.com/watch?v=GGaJsGu_5zA)

## Thanks

`Ser Chairzard#3463` wrote up the original POSt info, and `SJ#0701` sent the 148 notes link.
