---
title: course notes
subtitle: literacy -3
date: 2017-05-04T08:00:00-00:00
comments: false
---

*YMMV. Maybe helpful*.

## Course Notes

### Unknown

*I don't know who wrote these, but prolly helpful.*

[CSC148](https://www.teach.cs.toronto.edu/~csc148h/fall/notes/index.html)

### Tyler Holden 

*Currently a UTM prof, writes nice Calc notes. Love this guy.*

[MAT102 (Proof Primer + UTM POSt Course)](http://home.tykenho.com/LectureNotes102.pdf)

[MAT137 (Calculus! Highly recommended)](http://home.tykenho.com/LectureNotes137_Preview.pdf)

[MAT223 (Linear Algebra)](https://mcs.utm.utoronto.ca/~tholden/LectureNotes223.pdf)

### David Liu & Toniann Pitassi

*165 Prof IIRC.*

[CSC165 **(POSt Course)**](https://www.teach.cs.toronto.edu/~csc165h/winter/resources/csc165_notes.pdf)

### Vassos Hadzilacos

*(One of the upper year profs at UTSC for CS.)*

[CSC236/240/B36 (Theory of Computation. 2nd Year. May be a bit dry, but I like it.)](http://www.cs.toronto.edu/~vassos/b36-notes/notes.pdf)

## Open Source Textbooks and Misc.

[Interactive Linear Algebra](http://immersivemath.com/ila/index.html)

