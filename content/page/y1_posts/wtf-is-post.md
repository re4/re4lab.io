---
title: wtf is post
subtitle: oh god oh fuck
date: 2017-05-04T08:00:00-00:00
comments: false
---

POSt stand for Program Of Study. 
Simply put, when you make it into the faculty of Arts and Science (or UTSC/UTM),
you are actually `undeclared`, regardless of whatever you applied for.

Ignore if you're not in those faculties/are otherwise in a program where they
explicitly state first years enter the program directly. 
**This includes the engies.** Your "POSt" is trying to not design buildings 
that will lead to Prof. Cohen confiscating your iron ring.

If you are reading this, you prolly care about CS POSt more.

## How do I get CS POSt?

The requirement is different for every campus..

- SG: ~90 in Intro to CS 2 and Discrete Mathematics/Computational Reasoning/whatever they call it (CSC148 and CSC165). 
  Notice that this is *not* a guarantee, but instead they order everyone who applies
  and takes the top 550ish spots.
- SC: 3.0 in 6 courses. They are calc 1 (MATA31), calc 2 (MATA37), discrete 
  maths (CSCA67), lin alg 1 (MATA22) intro to CS 1 (CSCA08), intro to CS 2 
  (CSCA48). Note that A08 and A31 are weighted at 0.5.
- M: ~73 in (I think) CSC148 and MAT102. IIRC you also need a 3.0 CGPA. 
  Not too sure about this one, so go check the links at the bottom.

Notice that you are *guaranteed* to get into CS for SC/M if you make those requirements.

St. George... maybe not.

## So what does it actually mean if I make it in for CS from high school?

You get autoenrolled in courses. That's literally it for now. UTSG used to make
the in-stream cutoff lower, but not anymore.

## Can I try again if I don’t make it the first time?

Firstly, you can always drop and retake courses that you’re doing bad in.
For SG (and maybe M, idk) you can retake any course once for the sake of Post.
For SC, you can’t retake any course you passed so be careful.

## Links

[UTSG](http://web.cs.toronto.edu/program/ugrad/admission.htm)

[UTSC](https://www.utsc.utoronto.ca/cms/computer-science-1)

[UTM](https://www.utm.utoronto.ca/math-cs-stats/current-students/computer-science)
