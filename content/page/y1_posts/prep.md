---
title: prep
subtitle: prop
date: 2017-05-04T08:00:00-00:00
comments: false
---

# Burbulla's

*IIRC, the author is an eng prof, but... close enough. Give em a shot.
Solutions are somewhere on his site.*

[Basic Algebra](https://www.math.toronto.edu/burbulla/basicalgebra.pdf)

[Functions](https://www.math.toronto.edu/burbulla/functions.pdf)

[Trig](https://www.math.toronto.edu/burbulla/trigonometry.pdf)

[Logs and Exponentials](https://www.math.toronto.edu/burbulla/logsandexponentials.pdf)

# UfT Resources

*ty gerty*

[Math Dept's Math Prep](https://www.math.toronto.edu/preparing-for-calculus/)