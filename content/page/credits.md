---
title: credits
subtitle: never forget those who helped you on your way, even if they are gone now.
date: 2017-05-04T00:00:00-00:00
---

[GitLab](gitlab.com) was used to host this and [Hugo](gohugo.io) generated this crap.

The theme used was Ivan Chou's [Vec](https://github.com/IvanChou/hugo-theme-vec).

I used to use [GitGud](gitgud.io) to host the first year guide in markdown.

![GNOCCHI I DONT FEEL SO GOOD](/img/gnocchi.jpg)

This thing was public domain.
I can't find the author though, sadly.

