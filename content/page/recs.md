---
title: random recs
date: 2017-05-04T23:00:00-00:00
---

Some (common) recommendations I make.

- [cock.li](https://cock.li/) is a lovely mail host which manages my `re@waifu.club` email.
- [Antergos](https://antergos.com/) saves you time when you're too lazy to fuck with 999 repos or OS versions.
  Arch is much more stable than before, and upgrading major versions of Debian/Ubuntu/Fedora feel annoying.
- [mpv](https://mpv.io) is a 10/10 media player. It's great. Lightweight too.
- [GitGud.io](https://gitgud.io)... I don't know why it's here. Why not though lol.
- A certain [CD album](https://www.amazon.ca/Ram-Ranch-Grant-MacDonald/dp/B009H43Z9S) is great.
  Buy it today. (Please no).
- Corporate pulls are goat, they're cheap, maintainable, durable, and usually slower to get obsolete compared to consumer grade computers. Especially great with GNU/Linux.
- Actually, just install GNU/Linux.
- Though not for everyone, I like the Tin Audio T2s.
